<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220825081044 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE archive ADD user_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE archive ADD CONSTRAINT FK_D5FC5D9C9D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_D5FC5D9C9D86650F ON archive (user_id_id)');
        $this->addSql('ALTER TABLE user CHANGE sexe sexe enum(\'homme\',\'femme\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE archive DROP FOREIGN KEY FK_D5FC5D9C9D86650F');
        $this->addSql('DROP INDEX IDX_D5FC5D9C9D86650F ON archive');
        $this->addSql('ALTER TABLE archive DROP user_id_id');
        $this->addSql('ALTER TABLE user CHANGE sexe sexe VARCHAR(255) DEFAULT NULL');
    }
}
