<?php

namespace App\Controller;

use App\Entity\Archive;
use App\Repository\UserRepository;
use App\Repository\ArchiveRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArchiveController extends AbstractController
{
    /**
     * @Route("/profil", name="app_archive")
     */
    public function index(UserRepository $userRepository, ArchiveRepository $archiveRepository): Response
    {  
        $truc = $archiveRepository->findAll();
        dump($truc);
        return $this->render('archive/index.html.twig', [
            'controller_name' => 'ArchiveController',
            'user'=>$this->getUser(),
            'archives'=>$archiveRepository->findBy(['user_id'=>$this->getUser()->getId()])
        ]);
    }

    /**
     * @Route("/profil/save", name="app_archive_save", methods={"GET","POST"})
     */
    public function save(ArchiveRepository $archiveRepository, Request $request) :Response
    {
        dump($request);
        $archive = new Archive();
        $archive->setDate(new \DateTime(strtotime($request->request->get("daT"))));
        $archive->setPoids(floatval($request->request->get("poI")));
        $archive->setTaille(floatval($request->request->get("taI")));
        $archive->setImc(floatval($request->request->get("imC")));
        $archive->setMetaBaseRepos(floatval($request->request->get("mbR")));
        $archive->setMetaBaseActif(floatval($request->request->get("mbA")));
        $archive->setCommentaires($request->request->get("coM"));
        $archive->setUserId($this->getUser());
        

        $archiveRepository->add($archive, true);


        dump($archive);




        return $this->redirectToRoute('app_archive', [], Response::HTTP_SEE_OTHER);
        



    }
}
